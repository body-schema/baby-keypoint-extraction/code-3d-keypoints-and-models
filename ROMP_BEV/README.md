# 1. Prepare SMPL Model Files
Firstly, please register and download: 
a. Meta data from [this link](https://github.com/Arthur151/ROMP/releases/download/V2.0/smpl_model_data.zip). Please unzip it, then we get a folder named "smpl_model_data."
b. SMPL model file (SMPL_NEUTRAL.pkl) from "Download version 1.1.0 for Python 2.7 (female/male/neutral, 300 shape PCs)" in [official website](https://smpl.is.tue.mpg.de/). Please unzip it and move the SMPL_NEUTRAL.pkl from extracted folder into the "smpl_model_data" folder.
c. Download SMIL model file (DOWNLOAD SMIL) from [official website](https://www.iosb.fraunhofer.de/en/competences/image-exploitation/object-recognition/sensor-networks/motion-analysis.html). Please unzip and put it into the "smpl_model_data" folder, so we have "smpl_model_data/smil/smil_web.pkl".   
Then we can get a folder in structure like this:
```
|-- smpl_model_data
|   |-- SMPL_NEUTRAL.pkl
|   |-- J_regressor_extra.npy
|   |-- J_regressor_h36m.npy
|   |-- smpl_kid_template.npy
|   |-- smil
|   |-- |-- smil_web.pkl
```
# 2. Docker Build
Dockerfile slightly based on the official Dockerfile from May 2022
https://github.com/Arthur151/ROMP/blob/master/Dockerfile

```
docker build --rm -t romp .
```

# 3. Docker Start-Up
```
sudo docker run --rm --gpus all -it -e="DISPLAY" -v=/tmp/.X11-unix:/tmp/.X11-unix:rw -v /home/icub:/home/icub -v romp /bin/bash
```
Replace `home/icub` by user's `/home`.

Add `/media:/media` to give access to external hard drives.


# 4. Processing commands for ROMP and BEV
## Processing ROMP - single image
```
romp --mode=image -i $input_path -o $output_path --render_mesh 
```
## Processing ROMP - folder of images or a video
```
romp --mode=video -i $input_path -o $output_path --render_mesh --save_video
```
## Processing BEV - single image
```
bev --mode=image -i $input_path -o $output_path --show_items mesh
```
## Processing BEV - folder of images or a video
```
bev --mode=video -i $input_path -o $output_path --show_items mesh --save_video
```

# 5. Reformating the .npz file to the wanted .csv format (compatible with SMIL)
There are two scripts: The `romp_bev_to_csv_multiple_files.py` is for processing folder of .npz files, each for single frame. The `romp_bev_to_csv_single_file` is for processing single .npz file consisting of data of all frames.
