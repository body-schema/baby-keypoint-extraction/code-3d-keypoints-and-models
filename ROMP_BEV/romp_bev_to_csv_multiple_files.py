import numpy as np
import pandas as pd
import os
import glob

target = '/media/vojta/Elements/results/real/AA/8w/romp/00000000.npz'

img_extension = os.path.splitext(target)[1]

header = ["Nose_x_SMPL_3D", "Nose_y_SMPL_3D", "Nose_z_SMPL_3D", "Neck_x_SMPL_3D", "Neck_y_SMPL_3D", "Neck_z_SMPL_3D", "RShoulder_x_SMPL_3D", "RShoulder_y_SMPL_3D", "RShoulder_z_SMPL_3D", "RElbow_x_SMPL_3D", "RElbow_y_SMPL_3D", "RElbow_z_SMPL_3D", "RWrist_x_SMPL_3D", "RWrist_y_SMPL_3D", "RWrist_z_SMPL_3D", "LShoulder_x_SMPL_3D", "LShoulder_y_SMPL_3D", "LShoulder_z_SMPL_3D", "LElbow_x_SMPL_3D", "LElbow_y_SMPL_3D", "LElbow_z_SMPL_3D", "LWrist_x_SMPL_3D", "LWrist_y_SMPL_3D", "LWrist_z_SMPL_3D", "MidHip_x_SMPL_3D", "MidHip_y_SMPL_3D", "MidHip_z_SMPL_3D", "RHip_x_SMPL_3D", "RHip_y_SMPL_3D", "RHip_z_SMPL_3D", "RKnee_x_SMPL_3D", "RKnee_y_SMPL_3D", "RKnee_z_SMPL_3D", "RAnkle_x_SMPL_3D", "RAnkle_y_SMPL_3D", "RAnkle_z_SMPL_3D", "LHip_x_SMPL_3D", "LHip_y_SMPL_3D", "LHip_z_SMPL_3D", "LKnee_x_SMPL_3D", "LKnee_y_SMPL_3D", "LKnee_z_SMPL_3D", "LAnkle_x_SMPL_3D", "LAnkle_y_SMPL_3D", "LAnkle_z_SMPL_3D", "REye_x_SMPL_3D", "REye_y_SMPL_3D", "REye_z_SMPL_3D", "LEye_x_SMPL_3D", "LEye_y_SMPL_3D", "LEye_z_SMPL_3D", "REar_x_SMPL_3D", "REar_y_SMPL_3D", "REar_z_SMPL_3D", "LEar_x_SMPL_3D", "LEar_y_SMPL_3D", "LEar_z_SMPL_3D", "LBigToe_x_SMPL_3D", "LBigToe_y_SMPL_3D", "LBigToe_z_SMPL_3D", "LSmallToe_x_SMPL_3D", "LSmallToe_y_SMPL_3D", "LSmallToe_z_SMPL_3D", "LHeel_x_SMPL_3D", "LHeel_y_SMPL_3D", "LHeel_z_SMPL_3D", "RBigToe_x_SMPL_3D", "RBigToe_y_SMPL_3D", "RBigToe_z_SMPL_3D", "RSmallToe_x_SMPL_3D", "RSmallToe_y_SMPL_3D", "RSmallToe_z_SMPL_3D", "RHeel_x_SMPL_3D", "RHeel_y_SMPL_3D", "RHeel_z_SMPL_3D", "cam_trans_x", "cam_trans_y", "cam_trans_z", "Nose_x_SMPL_2D",	"Nose_y_SMPL_2D", "Neck_x_SMPL_2D", "Neck_y_SMPL_2D", "RShoulder_x_SMPL_2D", "RShoulder_y_SMPL_2D", "RElbow_x_SMPL_2D", "RElbow_y_SMPL_2D", "RWrist_x_SMPL_2D", "RWrist_y_SMPL_2D", "LShoulder_x_SMPL_2D", "LShoulder_y_SMPL_2D", "LElbow_x_SMPL_2D", "LElbow_y_SMPL_2D", "LWrist_x_SMPL_2D", "LWrist_y_SMPL_2D", "MidHip_x_SMPL_2D", "MidHip_y_SMPL_2D", "RHip_x_SMPL_2D", "RHip_y_SMPL_2D", "RKnee_x_SMPL_2D", "RKnee_y_SMPL_2D", "RAnkle_x_SMPL_2D", "RAnkle_y_SMPL_2D", "LHip_x_SMPL_2D", "LHip_y_SMPL_2D", "LKnee_x_SMPL_2D", "LKnee_y_SMPL_2D", "LAnkle_x_SMPL_2D", "LAnkle_y_SMPL_2D", "REye_x_SMPL_2D", "REye_y_SMPL_2D", "LEye_x_SMPL_2D", "LEye_y_SMPL_2D", "REar_x_SMPL_2D",	"REar_y_SMPL_2D", "LEar_x_SMPL_2D", "LEar_y_SMPL_2D", "LBigToe_x_SMPL_2D", "LBigToe_y_SMPL_2D", "LSmallToe_x_SMPL_2D", "LSmallToe_y_SMPL_2D", "LHeel_x_SMPL_2D", "LHeel_y_SMPL_2D", "RBigToe_x_SMPL_2D", "RBigToe_y_SMPL_2D", "RSmallToe_x_SMPL_2D", "RSmallToe_y_SMPL_2D", "RHeel_x_SMPL_2D",	"RHeel_y_SMPL_2D"]

df = pd.DataFrame(columns=header)

for filename in sorted(glob.glob(os.path.join(os.path.dirname(target), 
                                                      '*' + img_extension))):
    if os.path.basename(filename) == 'video_results.npz':
        continue

    results = np.load(filename,allow_pickle=True)['results'][()]
    for image in results['joints']:
        smpl3d = np.array([image[24], 
                   image[12],
                   image[17],
                   image[19],
                   image[21],
                   image[16],
                   image[18],
                   image[20],
                   image[0],
                   image[2],
                   image[5],
                   image[8],
                   image[1],
                   image[4],
                   image[7],
                   image[25],
                   image[26],
                   image[27],
                   image[28],
                   image[29],
                   image[30],
                   image[31],
                   image[32],
                   image[33],
                   image[34]
                   ])
    for image in results['pj2d_org']:
        smpl2d = np.array([image[24], 
                    image[12],
                    image[17],
                    image[19],
                    image[21],
                    image[16],
                    image[18],
                    image[20],
                    image[0],
                    image[2],
                    image[5],
                    image[8],
                    image[1],
                    image[4],
                    image[7],
                    image[25],
                    image[26],
                    image[27],
                    image[28],
                    image[29],
                    image[30],
                    image[31],
                    image[32],
                    image[33],
                    image[34]
                    ])
        cam_trans = results['cam_trans'][0]
        
        df = pd.concat([df,pd.DataFrame([np.concatenate((smpl3d.flatten(), cam_trans.flatten(), smpl2d.flatten()), axis=0)], 
                                        columns=header, index=[os.path.basename(filename)])], ignore_index = False)



df.to_csv('output.csv')  
