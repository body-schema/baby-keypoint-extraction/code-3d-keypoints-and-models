import pandas as pd
import numpy as np

col_name=["Nose",  "Neck",  "RShoulder",  "RElbow",  "RWrist",  "LShoulder",  "LElbow",  "LWrist",  "MidHip",\
"RHip", "RKnee", "RAnkle", "LHip", "LKnee", "LAnkle", "REye", "LEye", "REar", "LEar","LBigToe","LSmallToe",\
"LHeel", "RBigToe", "RSmallToe", "RHeel"]

## Add column names
list_2D = ['x', 'y']

SMPL_2D_colnames = ['{}_{}_{}_{}'.format(a, b, "SMPL", "2D") for a in col_name for b in list_2D]
GT_2D_colnames = ['{}_{}_{}_{}'.format(a, b, "GT", "2D") for a in col_name for b in list_2D]
SMPL_3D_2D_colnames = ['{}_{}_{}_{}_{}_{}'.format(a, b, "SMPL", "3D", "to", "2D") for a in col_name for b in list_2D]


path =  '/home/vojta/bakalarka/results/comparison/'
id = ['1','2','6','10','12']
methods = ['romp', 'bev', 'trace', '4dhumans', 'smplify_op', 'smplify_vit']


def prep_for_2D(data):
	data = data.set_index('Var1')
	if 'Neck_x_SMPL_2D' in data.columns:
		data = data[SMPL_2D_colnames]
	else:
		data = data[SMPL_3D_2D_colnames]
	#data = data[SMPL_3D_2D_colnames]
	data.columns = SMPL_2D_colnames
	return data

def prep_smplify(data):
	data = data.set_index('Var1')
	data = data[SMPL_2D_colnames]
	return data

def prep_gt(gt):
	gt = gt.set_index('Var1')
	gt = gt[GT_2D_colnames]
	gt.columns = SMPL_2D_colnames
	gt.replace(0, np.nan, inplace=True)
	return gt

def nm_mean(data):
	dist = ((data['Neck_x_SMPL_2D']- data['MidHip_x_SMPL_2D']).pow(2) + (data['Neck_y_SMPL_2D']- data['MidHip_y_SMPL_2D']).pow(2)).pow(1/2)
	#print(float(dist.mean()), float(dist.median()))
	dist = float(dist.median())
	return data.mul(1/dist), dist

def load_gt():
	gt = nm_mean(prep_gt(pd.read_csv(path + id[0] + '/gt/points_canonized_nice.csv')))
	for i in range(1,5):
		data, dist = nm_mean(prep_gt(pd.read_csv(path + id[i] + '/gt/points_canonized_nice.csv')))
		data.index += i*1000
		gt = gt._append(data)
	return gt

def load_method(method):
	data = nm_mean(prep_for_2D(pd.read_csv(path + id[0] + '/' + method + '/points_canonized_nice.csv')))
	
	for i in range(1,5):
		loaded = nm_mean(prep_for_2D(pd.read_csv(path + id[i] + '/' + method + '/points_canonized_nice.csv')))
		loaded.index += i*1000
		data = data._append(loaded)
	return data


def compare(gt, data):
	wanted_frames = list(data.index)
	gt = gt.loc[wanted_frames]
	results_2d = pd.DataFrame(columns=col_name)

	for i, col in enumerate(col_name):
		results_2d[col] = ((gt - data).pow(2)[SMPL_2D_colnames[2*i]] + (gt - data).pow(2)[SMPL_2D_colnames[2*i+1]]).pow(0.5)

	return results_2d

if __name__ == '__main__':
	gts = []
	for i in range(5):
		gts.append(prep_gt(pd.read_csv(path + id[i] + '/gt/points_canonized_nice.csv')))
	
	results_avg = []
	
	results = []
	detections = []
	for i in range(5):
		data = prep_for_2D(pd.read_csv(path + id[i] + '/' + methods[0] + '/points_canonized_nice.csv'))
		result = compare(gts[i], data)
		results.append(result.mean().mul(1/nm_mean(gts[i])[1]))
		detections.append(len(data.index))
	
	df = pd.DataFrame(sum(results[i].mul(detections[i]) for i in range(5)).mul(1/sum(detections)))
	results_avg.append(df[0].mean())
	df.columns = [methods[0]]





	
	for method in methods[1:]:
		results = []
		detections = []
		for i in range(5):
			data = prep_for_2D(pd.read_csv(path + id[i] + '/' + method + '/points_canonized_nice.csv'))
			result = compare(gts[i], data)
			results.append(result.mean().mul(1/nm_mean(gts[i])[1]))
			detections.append(len(data.index))
	
		r2 = sum(results[i].mul(detections[i]) for i in range(5)).mul(1/sum(detections))
		r2.name = method
		df = df.join(r2)
		results_avg.append(r2.mean())

	df = df.round(3).mul(100)
	df.to_csv('all_results/2D_t2_results.csv')

	print(results_avg)




