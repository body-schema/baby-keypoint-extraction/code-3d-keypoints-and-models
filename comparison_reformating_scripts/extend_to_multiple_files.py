import pandas as pd

method = 'smplify_vit'
if method == 'gt':
	method_path = '/' + method + '/points_gt_canonized.csv'
elif method == 'smplify_op' or method == 'smplify_vit':
	method_path = '/' + method + '/points_canonized.csv'
else:
	method_path = '/' + method + '/output_canonized.csv'

id = ['1','2','6','10','12']
path =  '/home/vojta/bakalarka/results/comparison/'


def prep_data(data, method):
	if method == 'bev' or method == 'romp':
		for i, frame in enumerate(data['Var1']):
			data['Var1'][i] = int(data['Var1'][i].split('.')[0].split('_')[1])
	elif method == '4dhumans':
		for i, frame in enumerate(data['Var1']):
			data['Var1'][i] = int(data['Var1'][i].split('/')[0].split('_')[1])
	elif method == 'gt':
		for i, frame in enumerate(data['Var1']):
			data['Var1'][i] = int(data['Var1'][i].split('_')[3])
	elif method == 'smplify_op' or method == 'smplify_vit':
		for i, frame in enumerate(data['Var1']):
			data['Var1'][i] = int(data['Var1'][i].split('_')[1])

	data = data.set_index('Var1')
	return data




final_data = prep_data(pd.read_csv(path + id[0] + method_path), method)

for i in range(1,5):
	data = prep_data(pd.read_csv(path + id[i] + method_path), method)
	data.index += i*1000
	final_data = final_data._append(data)

final_data.to_csv('all_results/' + method + '.csv')


