import pandas as pd
import numpy as np

methods = ['romp', 'bev', 'trace', '4dhumans', 'smplify_op', 'smplify_vit']

method = 'smplify_vit'

if method == 'gt':
	method_path = '/' + method + '/points_gt_canonized.csv'
elif method == 'smplify_op' or method == 'smplify_vit':
	method_path = '/' + method + '/points_canonized.csv'
else:
	method_path = '/' + method + '/output_canonized.csv'

path =  '/home/vojta/bakalarka/results/comparison/all_results/'


col_name=["Nose",  "Neck",  "RShoulder",  "RElbow",  "RWrist",  "LShoulder",  "LElbow",  "LWrist",  "MidHip",\
"RHip", "RKnee", "RAnkle", "LHip", "LKnee", "LAnkle", "REye", "LEye", "REar", "LEar","LBigToe","LSmallToe",\
"LHeel", "RBigToe", "RSmallToe", "RHeel"]

## Add column names
list_3D  = ['x', 'y', 'z']
SMPL_3D_colnames = ['{}_{}_{}_{}'.format(a, b, "SMPL", "3D") for a in col_name for b in list_3D]
gt_3D_colnames = ['{}_{}_{}_{}'.format(a, b, "GT", "3D") for a in col_name for b in list_3D]



def prep_for_3D(data):
    data = data.set_index('Var1')
    data = data[SMPL_3D_colnames]
    return data

def prep_gt(gt):
    gt = gt.set_index('Var1')
    gt = gt[gt_3D_colnames]
    
    gt.columns = SMPL_3D_colnames
    gt.replace(0, np.nan, inplace=True)
    gt['Neck_x_SMPL_3D'] = 0
    gt['Neck_y_SMPL_3D'] = 0
    gt['Neck_z_SMPL_3D'] = 0
    return gt

def compare(gt, data):
    wanted_frames = list(data.index)
    gt = gt.loc[wanted_frames]
    results_3d = pd.DataFrame(columns=col_name)
    results_2d = pd.DataFrame(columns=col_name)

    for i, col in enumerate(col_name):
        results_3d[col] = ((gt - data).pow(2)[SMPL_3D_colnames[3*i]] + (gt - data).pow(2)[SMPL_3D_colnames[3*i+1]] + (gt - data).pow(2)[SMPL_3D_colnames[3*i+2]]).pow(0.5)
        results_2d[col] = ((gt - data).pow(2)[SMPL_3D_colnames[3*i]] + (gt - data).pow(2)[SMPL_3D_colnames[3*i+1]]).pow(0.5)

    return results_3d, results_2d




if __name__ == '__main__':
    #romp = pd.read_csv(path+'romp.csv')
    gt = pd.read_csv(path+'gt.csv')
    
    #romp = prep_for_3D(romp)
    gt = prep_gt(gt)

    #res_3d, res_2d = compare(gt, romp)
    #print(f'Euclidean 3D: {res_3d.stack().dropna().mean()}\n')
    #print(res_3d.mean())
    #results = res_3d.mean()
    #results.type()
    #results.to_csv('experiment.csv')
    avgs3D = []
    avgs2D = []
    
    data = prep_for_3D(pd.read_csv(path + methods[0] + '.csv'))
    res_3d, res_2d = compare(gt, data)
    
    r3  = res_3d.mean()
    r3.name = methods[0]
    df3 = r3.to_frame()

    r2 = res_2d.mean()
    r2.name = methods[0]
    df2 = r2.to_frame()
    
    avgs3D.append(res_3d.stack().dropna().mean())
    avgs2D.append(res_2d.stack().dropna().mean())



    for i in methods[1:]:
        data = prep_for_3D(pd.read_csv(path + i + '.csv'))
        res_3d, res_2d = compare(gt, data)

        r3  = res_3d.mean()
        r3.name = i
        df3 = df3.join(r3)

        r2 = res_2d.mean()
        r2.name = i
        df2 = df2.join(r2)

        avgs3D.append(res_3d.stack().dropna().mean())
        avgs2D.append(res_2d.stack().dropna().mean())

    df3 = df3.round(3).mul(100)
    df2 = df2.round(3).mul(100)

    df3.to_csv('all_results/3D_results.csv')
    with open('all_results/3D_results.tex', 'w') as f:
        f.write(df3.to_latex())

    df2.to_csv('all_results/2D_results.csv')
    with open('all_results/2D_results.tex', 'w') as f:
        f.write(df2.to_latex())

    print(avgs3D)
    print(avgs2D)