`extend_to_multiple_files.py` merges multiple result files of the same method together

`mpjpe_new.py` computes MPJPE in 3D and MPJPE in 2D (type I)

`mpjpe_2_t2.py` computes MPJPE in 2D (type II)

`std.py` computes bone length standard deviation
