import pandas as pd
import numpy as np

methods = ['romp', 'bev', 'trace', '4dhumans', 'smplify_op', 'smplify_vit']

method = 'smplify_vit'

if method == 'gt':
	method_path = '/' + method + '/points_gt_canonized.csv'
elif method == 'smplify_op' or method == 'smplify_vit':
	method_path = '/' + method + '/points_canonized.csv'
else:
	method_path = '/' + method + '/output_canonized.csv'

path =  '/home/vojta/bakalarka/results/comparison/all_results/'

col_name=["Nose",  "Neck",  "RShoulder",  "RElbow",  "RWrist",  "LShoulder",  "LElbow",  "LWrist",  "MidHip",\
"RHip", "RKnee", "RAnkle", "LHip", "LKnee", "LAnkle", "REye", "LEye", "REar", "LEar","LBigToe","LSmallToe",\
"LHeel", "RBigToe", "RSmallToe", "RHeel"]

## Add column names
list_3D  = ['x', 'y', 'z']
SMPL_3D_colnames = ['{}_{}_{}_{}'.format(a, b, "SMPL", "3D") for a in col_name for b in list_3D]
gt_3D_colnames = ['{}_{}_{}_{}'.format(a, b, "GT", "3D") for a in col_name for b in list_3D]



def prep_for_3D(data):
    data = data.set_index('Var1')
    data = data[SMPL_3D_colnames]
    return data


rs = [SMPL_3D_colnames[x] for x in [6,7,8]]
ls = [SMPL_3D_colnames[x] for x in [15,16,17]]
re = [SMPL_3D_colnames[x] for x in [9,10,11]]
le = [SMPL_3D_colnames[x] for x in [18,19,20]]
rw = [SMPL_3D_colnames[x] for x in [12,13,14]]
lw = [SMPL_3D_colnames[x] for x in [21,22,23]]

rh = [SMPL_3D_colnames[x] for x in [27,28,29]]
lh = [SMPL_3D_colnames[x] for x in [36,37,38]]
rk = [SMPL_3D_colnames[x] for x in [30,31,32]]
lk = [SMPL_3D_colnames[x] for x in [39,40,41]]
ra = [SMPL_3D_colnames[x] for x in [33,34,35]]
la = [SMPL_3D_colnames[x] for x in [42,43,44]]



if __name__ == '__main__':
	stds = {}
	for i in methods:
		data = prep_for_3D(pd.read_csv(path + i + '.csv'))

		Rforearm = ((data[re[0]] - data[rw[0]]).pow(2) + (data[re[1]] - data[rw[1]]).pow(2) + (data[re[2]] - data[rw[2]]).pow(2)).pow(0.5)
		Relbow = ((data[re[0]] - data[rs[0]]).pow(2) + (data[re[1]] - data[rs[1]]).pow(2) + (data[re[2]] - data[rs[2]]).pow(2)).pow(0.5)

		Lforearm = ((data[le[0]] - data[lw[0]]).pow(2) + (data[le[1]] - data[lw[1]]).pow(2) + (data[le[2]] - data[lw[2]]).pow(2)).pow(0.5)
		Lelbow = ((data[le[0]] - data[ls[0]]).pow(2) + (data[le[1]] - data[ls[1]]).pow(2) + (data[le[2]] - data[ls[2]]).pow(2)).pow(0.5)

		Rcalf = ((data[ra[0]] - data[rk[0]]).pow(2) + (data[ra[1]] - data[rk[1]]).pow(2) + (data[ra[2]] - data[rk[2]]).pow(2)).pow(0.5)
		Rthigh = ((data[rh[0]] - data[rk[0]]).pow(2) + (data[rh[1]] - data[rk[1]]).pow(2) + (data[rh[2]] - data[rk[2]]).pow(2)).pow(0.5)

		Lcalf = ((data[la[0]] - data[lk[0]]).pow(2) + (data[la[1]] - data[lk[1]]).pow(2) + (data[la[2]] - data[lk[2]]).pow(2)).pow(0.5)
		Lthigh = ((data[lh[0]] - data[lk[0]]).pow(2) + (data[lh[1]] - data[lk[1]]).pow(2) + (data[lh[2]] - data[lk[2]]).pow(2)).pow(0.5)

		stds[i] = [Rforearm.std(ddof=0), Lforearm.std(ddof=0), Relbow.std(ddof=0), Lelbow.std(ddof=0), Rthigh.std(ddof=0), Lthigh.std(ddof=0), Rcalf.std(ddof=0), Lcalf.std(ddof=0)]

	print(stds)
	df = pd.DataFrame(stds)
	print(df)
	print(df.mean().round(3))
	df = df.round(3)
	df.to_csv('all_results/stds.csv')

