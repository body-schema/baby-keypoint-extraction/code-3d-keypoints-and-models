# 4D Humans Installation

```
conda create --name 4D-humans python=3.10
conda activate 4D-humans
pip install torch==1.12.1+cu102 torchvision==0.13.1+cu102 torchaudio==0.12.1 --extra-index-url https://download.pytorch.org/whl/cu102
pip install numpy==1.23.1
pip install -U memory_profiler

sudo apt-get install g++-7
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 7
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-7 7
sudo update-alternatives --config gcc

pip install -e .[all]
```

# 4D Humans Inference

## Modifications to the demo script


## Running 4D Humans
One must enter the 4D Humans conda environment with `conda activate 4D-humans`.

It is needed to prepare the output folder to contain other folders in the following structure:
```
|-- output_path
|   |-- imgs
|   |-- imgs_keypoints
|   |-- output_raw
```
To run with memory profiler, insert `mprof run --include-children --multiprocess --output $output_path/mprofile.dat` before the following processing command.
```
python demo.py --img_folder $input_path --out_folder /output_path --batch_size=48 --full_frame
```

# Changes to 4D Humans (SMPL) files to output SMIL
`smplx.body_models.py`:
 - Row 55 changed 10 to 20
 - Row 146 changed 10 to 20

`smpl_head.py`:
 - Row 31 (npose + 10 + 3) changed to (npose + 20 + 3)
 - Row 40 changed 10 to 20

`discriminator.py`:
 - Row 30 (10,10) changed to (20,10)
